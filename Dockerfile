FROM ubuntu:22.04

#===============================================================================
# Ubuntu setup
#===============================================================================

RUN \
    apt-get update -y && \
        DEBIAN_FRONTEND=noninteractive apt-get install -y \
        bash \
        build-essential \
        cmake \
        clang \
        curl \
        dnsutils \
        git \
        libssl-dev \
        lsb-release \
        mc \
        nano \
        net-tools \
        openssh-server \
        pkg-config \
        protobuf-compiler \
        sudo \
        systemd-coredump \
        wget

ENV HOME /home/peerplays
RUN \
    useradd -rm -d /home/peerplays -s /bin/bash -g root -G sudo -u 1000 peerplays && \
    echo "peerplays  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/peerplays && \
    chmod 440 /etc/sudoers.d/peerplays && \
    service ssh start && \
    echo 'peerplays:peerplays' | chpasswd

# SSH
EXPOSE 22

# SSH server
#CMD ["/usr/sbin/sshd","-D"]

#===============================================================================

WORKDIR /home/peerplays/src

#===============================================================================
# Rust setup
#===============================================================================

RUN \
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | bash -s -- -y

#===============================================================================
# Peerplays 2.0 setup
#===============================================================================

## Clone Peerplays 2.0
#RUN \
#    git clone https://gitlab.com/PBSA/Peerplays_2.0/discovery.git && \
#    git branch --show-current && \
#    git log --oneline -n 5

# Add local source
ADD . peerplays

# Build Peerplays 2.0
RUN \
    . $HOME/.cargo/env && \
    cd peerplays && \
    cargo build --release && \
    mkdir /home/peerplays/peerplays-network && \
    cp /home/peerplays/src/peerplays/target/release/peerplays /home/peerplays/peerplays-network/peerplays && \
    rm -rf /home/peerplays/src

WORKDIR /home/peerplays/peerplays-network

# Peerplays 2.0 Prometheus
EXPOSE 9615
# Peerplays 2.0 RPC
EXPOSE 9933
# Peerplays 2.0 WS
EXPOSE 9944
# Peerplays 2.0 P2P
EXPOSE 30333

# Peerplays 2.0
CMD ["./peerplays", "--base-path", "./", "--dev", "--port", "30333", "--rpc-external", "--rpc-methods", "unsafe"]


[![Lines of Code](https://sonarqube.peerplays.tech/api/project_badges/measure?project=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ&metric=ncloc)](https://sonarqube.peerplays.tech/dashboard?id=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ)
[![Maintainability Rating](https://sonarqube.peerplays.tech/api/project_badges/measure?project=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ&metric=sqale_rating)](https://sonarqube.peerplays.tech/dashboard?id=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ)
[![Security Rating](https://sonarqube.peerplays.tech/api/project_badges/measure?project=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ&metric=security_rating)](https://sonarqube.peerplays.tech/dashboard?id=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ)
[![Bugs](https://sonarqube.peerplays.tech/api/project_badges/measure?project=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ&metric=bugs)](https://sonarqube.peerplays.tech/dashboard?id=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ)
[![Vulnerabilities](https://sonarqube.peerplays.tech/api/project_badges/measure?project=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ&metric=vulnerabilities)](https://sonarqube.peerplays.tech/dashboard?id=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ)
[![Quality Gate Status](https://sonarqube.peerplays.tech/api/project_badges/measure?project=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ&metric=alert_status)](https://sonarqube.peerplays.tech/dashboard?id=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ)
[![Technical Debt](https://sonarqube.peerplays.tech/api/project_badges/measure?project=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ&metric=sqale_index)](https://sonarqube.peerplays.tech/dashboard?id=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ)
[![Coverage](https://sonarqube.peerplays.tech/api/project_badges/measure?project=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ&metric=coverage)](https://sonarqube.peerplays.tech/dashboard?id=PBSA_Peerplays_2.0_discovery_AYez_R3t-9FJlcNOn8zQ)
[![pipeline status](https://gitlab.com/PBSA/Peerplays_2.0/discovery/badges/dev/pipeline.svg)](https://gitlab.com/PBSA/Peerplays_2.0/discovery/-/commits/master)

# Peerplays 2.0

Peerplays 2.0 is a next-generation peerplays

## Getting Started

Officially supported OS is Ubuntu 20.04.

### Install Rust
Install dependencies
```
sudo apt install build-essential cmake clang curl git protobuf-compiler
```

Install Rust with the following command
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

For latest Rust install instructions, visit: https://www.rust-lang.org/learn/get-started

The Rust compiler version used is defined in the `rust-toolchain.toml` file, located in a project root. If required version is not present on the system, it will be installed on first build.

Checkout compiler versions
```
rustup show

# Output should be similar to this
Default host: x86_64-unknown-linux-gnu
rustup home:  /home/ubuntu/.rustup

installed toolchains
--------------------

stable-x86_64-unknown-linux-gnu
nightly-2021-03-01-x86_64-unknown-linux-gnu
nightly-2022-11-14-x86_64-unknown-linux-gnu
nightly-2023-05-23-x86_64-unknown-linux-gnu (default)
nightly-x86_64-unknown-linux-gnu
1.69-x86_64-unknown-linux-gnu
1.69.0-x86_64-unknown-linux-gnu

installed targets for active toolchain
--------------------------------------

wasm32-unknown-unknown
x86_64-unknown-linux-gnu

active toolchain
----------------

nightly-2023-05-23-x86_64-unknown-linux-gnu (default)
rustc 1.71.0-nightly (8b4b20836 2023-05-22)
```

Update Rust with the following command
```
rustup update
```

### Rust build system
```
cargo build         # Builds project
cargo run           # Runs project
cargo test          # Runs tests
cargo doc           # Builds documentation for the project
cargo --version     # Displays cargo and Rust versions
```

### Building Peerplays 2.0
```
git clone https://gitlab.com/PBSA/Peerplays_2.0/discovery.git
cd discovery

# Build debug version
cargo build

# Build release version
cargo build --release
```

### Running Peerplays 2.0
Depending on a build mode, debug or release, executables will be stored at the following folders
```
target/debug
target/release
```

#### Start the node
There are three flavors of the node: dev, testnet and mainnet.
- dev is used for software development. It uses single validator and default development accounts
- testnet is used for running public or local testnet. It uses 11 validators and predefined accounts
- mainnet is used for running public mainnet. It uses 11 validators and predefined accounts
```
# This will start dev validator node
target/debug/peerplays --base-path ./peerplays_data_dir/ --dev

# This will start a dev sync node, without validator
target/debug/peerplays --base-path ./peerplays_data_dir/

# This will start testnet validator node
target/debug/peerplays --base-path ./peerplays_data_dir/ --chain testnet --validator

# This will start testnet sync node, withour validator
target/debug/peerplays --base-path ./peerplays_data_dir/ --chain testnet

# This will start mainnet validator node
target/debug/peerplays --base-path ./peerplays_data_dir/ --chain mainnet --validator

# This will start mainnet sync node, withour validator
target/debug/peerplays --base-path ./peerplays_data_dir/ --chain mainnet
```

In order to connect sync node to a validator node, additional arguments are needed. Note that your validator node identity might be different. Get it from validator node logs.
```
Peerplays 2.0 Node
version 3.0.0-dev-f34eb16f646
by Peerplays 2.0 discovery, 2017-2023
Chain specification: Peerplays 2.0
Node name: phobic-digestion-1053
Role: AUTHORITY
Database: RocksDb at /tmp/substrateqzggxP/chains/dev/db/full
Native runtime: node-268 (peerplays-node-0.tx2.au10)
generated 1 npos voters, 1 from validators and 0 nominators
generated 1 npos targets
Initializing Genesis block/state (state: 0x8b53…e1cf, header-hash: 0xf9f2…cc7c)
Loading GRANDPA authority set from genesis on what appears to be first startup.
Creating empty BABE epoch changes on what appears to be first startup.
Running Frontier DB migration from version 1 to version 2. Please wait.
Successful Frontier DB migration from version 1 to version 2 (0 entries).
Using default protocol ID "sup" because none is configured in the chain specs
Local node identity is: 12D3KooWHnuGqHhKRqEEw59SPFFL8Ct9CtXRqypow9o9AKahqDqW    <---- HERE IT IS!!!
Operating system: linux
CPU architecture: x86_64
...
```

Start validator node and connect sync node to it
```
# Start dev validator node
target/debug/peerplays --base-path ./peerplays_data_dir/ --dev

# Start dev sync node (your node identity might be different, you can pick it up from dev validator node)
target/debug/peerplays --base-path ./peerplays_data_dir2/ --bootnodes /ip4/127.0.0.1/tcp/30333/p2p/12D3KooWHnuGqHhKRqEEw59SPFFL8Ct9CtXRqypow9o9AKahqDqW
```

If your node do not build and finalizes blocks, you need to add validator keys to the node keystore. Software does not support running multiple validators from a single node, so when running testnet or mainnet configuration, 11 running nodes are needed, and different validator keys should be imported to each one of them.
```
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "main awake seek truly rebel pluck aunt receive height cook bless weasel" --key-type gran --scheme Ed25519
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "main awake seek truly rebel pluck aunt receive height cook bless weasel" --key-type babe --scheme Sr25519
```

### Peerplays 2.0 docker image
Install docker, and add current user to docker group
```
sudo apt install docker.io
sudo usermod -a -G docker $USER

# You need to restart your shell session, to apply group membership
# Type 'groups' to verify that you are a member of a docker group
```

Running official docker image, built from master branch
```
docker run -p 127.0.0.1:9615:9615/tcp -p 127.0.0.1:9933:9933/tcp -p 127.0.0.1:9944:9944/tcp -p 127.0.0.1:30333:30333/tcp registry.gitlab.com/pbsa/peerplays_2.0/discovery/master:latest
```

Building docker image from the local source code
```
git clone https://gitlab.com/PBSA/Peerplays_2.0/discovery.git
cd discovery
docker build -t peerplays2 .
```

Running Peerplays 2.0 from docker
```
docker run -p 127.0.0.1:9615:9615/tcp -p 127.0.0.1:9933:9933/tcp -p 127.0.0.1:9944:9944/tcp -p 127.0.0.1:30333:30333/tcp peerplays2:latest
```

#### Connect blockchain explorer
Peerplays 2.0 requires customized Polkadot UI. This is provided in Peerplays 2.0 QA Environment.

Starting customized Polkadot UI from Peerplays 2.0 QA Environment
```
cd discovery-qa
docker-compose up discovery-polkadot-ui
```

Once Polkadot UI is running:
- Visit http://localhost:3000
- If you are not immediatelly connected to the local node, click on the network name in the top left corner
- Select Development/Local node, or create custom endpoint with the address ws://127.0.0.1:9944, and click Switch
- Enable in-browser account creation - go to Settings page, section account options and set "in-browser account creation" to "Allow local in-browser account storage" and click Save

#### Connect Ink smart contracts UI
Starting Contracts UI from Peerplays 2.0 QA Environment
```
cd discovery-qa
docker-compose up discovery-contracts-ui
```

Once Contracts UI is running:
- Visit http://localhost:8081
- If you are not immediatelly connected to the local node, click on the network name in the top left corner
- Select Local node
- Make sure you have a working wallet, like Polkadot extension or Talisman

#### Connect Substrate telemetry UI
In order to access telemetry data, node must be started with additional parameter `--telemetry-url`.

Starting Substrate telemetry UI from Peerplays 2.0 QA Environment
```
cd discovery-qa
docker-compose up discovery-substrate-telemetry-core discovery-substrate-telemetry-shard discovery-substrate-telemetry-frontend
```

Once Substrate telemetry UI is running:
- Start your node with additional parameter `--telemetry-url 'ws://127.0.0.1:8001/submit 0'`
- Visit http://localhost:8080

#### Connect Prometheus
In order to access telemetry data, node must be started with additional parameter `--prometheus-external`.

Starting Prometheus from Peerplays 2.0 QA Environment
```bash
cd discovery-qa
docker-compose up discovery-prometheus
```

Once Prometheus is running:
- Start your node with additional parameter `--prometheus-external`
- Visit http://localhost:9090
- Click on menu item Status/Targets

#### Connect Grafana
Before starting Grafana make sure Prometheus is configured.

Starting Grafana from Peerplays 2.0 QA Environment
```bash
cd discovery-qa
docker-compose up discovery-grafana
```

Prometheus container will be started automatically, if not already running.

Once the Grafana is running:
- Visit http://localhost:9999
- Click on menu item Home/Dashboards
- Checkout preconfigured dashboard `discovery-qa/localnet`

#### Basic subcommands
```
target/debug/peerplays benchmark
    Sub-commands concerned with benchmarking. The pallet benchmarking moved to the `pallet` sub-command
target/debug/peerplays build-spec
    Build a chain specification
target/debug/peerplays chain-info
    Db meta columns information
target/debug/peerplays check-block
    Validate blocks
target/debug/peerplays export-blocks
    Export blocks
target/debug/peerplays export-state
    Export the state of a given block into a chain spec
target/debug/peerplays help
    Print this message or the help of the given subcommand(s)
target/debug/peerplays import-blocks
    Import blocks
target/debug/peerplays inspect
    Decode given block or extrinsic using current native runtime.
target/debug/peerplays key
    Key management cli utilities
target/debug/peerplays purge-chain
    Remove the whole chain
target/debug/peerplays revert
    Revert the chain to a previous state
target/debug/peerplays sign
    Sign a message, with a given (secret) key
target/debug/peerplays try-runtime
    Try some command against runtime state. Note: `try-runtime` feature must be enabled
target/debug/peerplays vanity
    Generate a seed that provides a vanity address
target/debug/peerplays verify
    Verify a signature for a message, provided on STDIN, with a given (public or secret) key
```

Each subcommand has available help
```
target/debug/peerplays key --help
```

#### Key generation
```
target/debug/peerplays key generate

# Output will be similar to this
Secret phrase:       main awake seek truly rebel pluck aunt receive height cook bless weasel
  Network ID:        substrate
  Secret seed:       0x0620f2aa019c28d0e725b505e805312f31a576f765b93623695896aa0d92ee88
  Public key (hex):  0x942c7b1ea2daf62a5315f487281df46c827c3c426a317d0781d34e19a762475f
  Account ID:        0x942c7b1ea2daf62a5315f487281df46c827c3c426a317d0781d34e19a762475f
  Public key (SS58): 5FQzA6jo3ZzFP1C1epf4xjEj1rdggU99uy8ZQtdWFd6QdEUv
  SS58 Address:      5FQzA6jo3ZzFP1C1epf4xjEj1rdggU99uy8ZQtdWFd6QdEUv
```

#### Testkeys
Test keys are listed in file named testkeys, in the project root.

#### Inserting keys into keystore
This is a list of commands to insert default set of test keys into a node keystore
```
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "main awake seek truly rebel pluck aunt receive height cook bless weasel" --key-type gran --scheme Ed25519
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "main awake seek truly rebel pluck aunt receive height cook bless weasel" --key-type babe --scheme Sr25519

target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "use clock lunar there health private pigeon oyster trigger half evolve slab" --key-type gran --scheme Ed25519
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "use clock lunar there health private pigeon oyster trigger half evolve slab" --key-type babe --scheme Sr25519

target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "blanket faith tired fitness wrong mouse duck raise gun flee tiger also" --key-type gran --scheme Ed25519
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "blanket faith tired fitness wrong mouse duck raise gun flee tiger also" --key-type babe --scheme Sr25519

target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "vault tilt guess neutral melody buddy embody priority wrong bronze mention juice" --key-type gran --scheme Ed25519
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "vault tilt guess neutral melody buddy embody priority wrong bronze mention juice" --key-type babe --scheme Sr25519

target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "dutch barely host rapid device fee define drip syrup clay come disease" --key-type gran --scheme Ed25519
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "dutch barely host rapid device fee define drip syrup clay come disease" --key-type babe --scheme Sr25519

target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "draw maid blast hurt anchor delay impulse task raw helmet sort brick" --key-type gran --scheme Ed25519
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "draw maid blast hurt anchor delay impulse task raw helmet sort brick" --key-type babe --scheme Sr25519

target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "beyond payment agree hat rapid acquire trouble fringe demise satoshi peace apart" --key-type gran --scheme Ed25519
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "beyond payment agree hat rapid acquire trouble fringe demise satoshi peace apart" --key-type babe --scheme Sr25519

target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "piano family squirrel glad oven remember impose skill wrong couple fade lizard" --key-type gran --scheme Ed25519
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "piano family squirrel glad oven remember impose skill wrong couple fade lizard" --key-type babe --scheme Sr25519

target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "warfare object accuse master wear wage tribe peace hospital maximum candy boy" --key-type gran --scheme Ed25519
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "warfare object accuse master wear wage tribe peace hospital maximum candy boy" --key-type babe --scheme Sr25519

target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "few sort attitude current exist render juice swift alcohol they energy setup" --key-type gran --scheme Ed25519
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "few sort attitude current exist render juice swift alcohol they energy setup" --key-type babe --scheme Sr25519

target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "soccer quote outside moment potato creek tenant enforce marble feel venue apart" --key-type gran --scheme Ed25519
target/debug/peerplays key insert --base-path ./peerplays_data_dir/ --suri "soccer quote outside moment potato creek tenant enforce marble feel venue apart" --key-type babe --scheme Sr25519
```

[package]
name = "staging-node-cli"
version = "3.0.0-dev"
authors.workspace = true
description = "Generic Peerplays 2.0 node implementation in Rust."
build = "build.rs"
edition.workspace = true
license = "GPL-3.0-or-later WITH Classpath-exception-2.0"
default-run = "peerplays"
homepage = "https://peerplays.com"
repository.workspace = true
publish = false

[lints]
workspace = true

[package.metadata.wasm-pack.profile.release]
# `wasm-opt` has some problems on linux, see
# https://github.com/rustwasm/wasm-pack/issues/781 etc.
wasm-opt = false

[package.metadata.docs.rs]
targets = ["x86_64-unknown-linux-gnu"]

[badges]
travis-ci = { repository = "paritytech/substrate" }
maintenance = { status = "actively-developed" }
is-it-maintained-issue-resolution = { repository = "paritytech/substrate" }
is-it-maintained-open-issues = { repository = "paritytech/substrate" }

# The same node binary as the `peerplays` (defined in the workspace `Cargo.toml`) binary,
# but just exposed by this crate here.
[[bin]]
name = "peerplays"
path = "bin/main.rs"
required-features = ["cli"]

[lib]
crate-type = ["cdylib", "rlib"]

[dependencies]
# third-party dependencies
array-bytes = "6.1"
clap = { version = "4.4.14", features = ["derive"], optional = true }
codec = { package = "parity-scale-codec", version = "3.6.1" }
serde = { version = "1.0.195", features = ["derive"] }
jsonrpsee = { version = "0.16.2", features = ["server"] }
futures = "0.3.21"
log = "0.4.17"
rand = "0.8"

cfg-if = "0.1"
coverage-helper = "0.1"

# primitives
sp-authority-discovery = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-consensus-babe = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
beefy-primitives = { package = "sp-consensus-beefy", git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
grandpa-primitives = { package = "sp-consensus-grandpa", git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-api = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-core = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-runtime = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-timestamp = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-inherents = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-keyring = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-keystore = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-consensus = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-transaction-storage-proof = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-io = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-mixnet = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-mmr-primitives = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
#sp-statement-store = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }

# client dependencies
sc-client-api = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-chain-spec = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-consensus = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-transaction-pool = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-transaction-pool-api = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-statement-store = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-network = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-network-common = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-network-sync = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-network-statement = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-consensus-slots = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-consensus-babe = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
beefy = { package = "sc-consensus-beefy", git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
grandpa = { package = "sc-consensus-grandpa", git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
mmr-gadget = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-rpc = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-basic-authorship = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-service = {  git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", default-features = false }
sc-telemetry = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-executor = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-authority-discovery = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-mixnet = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-sync-state-rpc = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-sysinfo = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-storage-monitor = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-offchain = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }

# frame dependencies
frame-benchmarking = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
frame-system = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
frame-system-rpc-runtime-api = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
pallet-assets = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
pallet-asset-conversion-tx-payment = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
pallet-asset-tx-payment = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
pallet-im-online = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", default-features = false }
pallet-skip-feeless-payment = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", default-features = false }

# peerplays-specific dependencies
peerplays-runtime = { path = "../runtime" }
node-executor = { path = "../executor" }
node-primitives = { path = "../primitives" }
node-rpc = { path = "../rpc" }

# CLI-specific dependencies
sc-cli = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", optional = true }
frame-benchmarking-cli = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", optional = true }
node-inspect = { package = "staging-node-inspect", path = "../inspect", optional = true }
try-runtime-cli = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", optional = true }
serde_json = "1.0.111"

# Frontier dependencies
fc-api = { git = "https://github.com/paritytech/frontier", branch = "polkadot-v1.6.0" }
fc-cli = { git = "https://github.com/paritytech/frontier", branch = "polkadot-v1.6.0" }
fc-consensus = { git = "https://github.com/paritytech/frontier", branch = "polkadot-v1.6.0"}
fc-db = { git = "https://github.com/paritytech/frontier", branch = "polkadot-v1.6.0" }
fc-mapping-sync = { git = "https://github.com/paritytech/frontier", branch = "polkadot-v1.6.0"}
fc-rpc = { git = "https://github.com/paritytech/frontier", branch = "polkadot-v1.6.0" }
fc-rpc-core = { git = "https://github.com/paritytech/frontier", branch = "polkadot-v1.6.0" }
fc-storage = { git = "https://github.com/paritytech/frontier", branch = "polkadot-v1.6.0" }
fp-account = { git = "https://github.com/paritytech/frontier", branch = "polkadot-v1.6.0" }
fp-dynamic-fee = { git = "https://github.com/paritytech/frontier", branch = "polkadot-v1.6.0" }
fp-evm = { git = "https://github.com/paritytech/frontier", branch = "polkadot-v1.6.0" }
fp-rpc = { git = "https://github.com/paritytech/frontier", branch = "polkadot-v1.6.0" }

sp-block-builder = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", default-features = false }
sp-offchain = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", default-features = false }
sp-session = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", default-features = false }
sp-transaction-pool = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", default-features = false }
pallet-transaction-payment-rpc-runtime-api = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", default-features = false }

[dev-dependencies]
sc-keystore = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-client-db = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-consensus = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-consensus-babe = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-consensus-epochs = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-service-test = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-block-builder = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-tracing = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-blockchain = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
futures = "0.3.21"
tempfile = "3.1.0"
assert_cmd = "2.0.2"
nix = { version = "0.26.1", features = ["signal"] }
regex = "1.6.0"
platforms = "3.0"
soketto = "0.7.1"
criterion = { version = "0.4.0", features = ["async_tokio"] }
tokio = { version = "1.22.0", features = ["macros", "parking_lot", "time"] }
tokio-util = { version = "0.7.4", features = ["compat"] }
wait-timeout = "0.2"
substrate-rpc-client = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
pallet-timestamp = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
#substrate-cli-test-utils = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }

wat = "1.0"
frame-support = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
node-testing = { path = "../testing" }
pallet-balances = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
pallet-contracts = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
#pallet-glutton = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
pallet-sudo = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
pallet-treasury = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
pallet-transaction-payment = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-application-crypto = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
pallet-root-testing = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-consensus-babe = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-externalities = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-keyring = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-runtime = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
serde_json = "1.0.111"
scale-info = { version = "2.10.0", features = ["derive", "serde"] }
sp-trie = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sp-state-machine = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }

[build-dependencies]
clap = { version = "4.4.14", optional = true }
clap_complete = { version = "4.0.2", optional = true }
node-inspect = { package = "staging-node-inspect", path = "../inspect", optional = true }
frame-benchmarking-cli = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", optional = true }
substrate-build-script-utils = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", optional = true }
substrate-frame-cli = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", optional = true }
try-runtime-cli = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", optional = true }
sc-cli = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0", optional = true }
pallet-balances = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }
sc-storage-monitor = { git = "https://github.com/paritytech/polkadot-sdk", branch = "release-polkadot-v1.6.0" }

[features]
default = [
	"cli",
	"rocksdb",
	"sql",
	"txpool",
	"rpc-binary-search-estimate",
]
cli = [
	"clap",
	"clap_complete",
	"frame-benchmarking-cli",
	"node-inspect",
	"sc-cli",
	"sc-service/rocksdb",
	"substrate-build-script-utils",
	"substrate-frame-cli",
	"try-runtime-cli",
]
rocksdb = [
	"sc-cli/rocksdb",
	"sc-service/rocksdb",
	"fc-cli/rocksdb",
	"fc-db/rocksdb",
	"fc-mapping-sync/rocksdb",
	"fc-rpc/rocksdb",
]
sql = [
	"fc-db/sql",
	"fc-mapping-sync/sql",
]
txpool = ["fc-rpc/txpool"]
rpc-binary-search-estimate = ["fc-rpc/rpc-binary-search-estimate"]

runtime-benchmarks = [
	"frame-benchmarking-cli/runtime-benchmarks",
	"frame-benchmarking/runtime-benchmarks",
	"frame-support/runtime-benchmarks",
	"frame-system/runtime-benchmarks",
	"peerplays-runtime/runtime-benchmarks",
	"node-inspect?/runtime-benchmarks",
	"pallet-asset-tx-payment/runtime-benchmarks",
	"pallet-assets/runtime-benchmarks",
	"pallet-balances/runtime-benchmarks",
	"pallet-contracts/runtime-benchmarks",
	#"pallet-glutton/runtime-benchmarks",
	"pallet-im-online/runtime-benchmarks",
	"pallet-skip-feeless-payment/runtime-benchmarks",
	"pallet-sudo/runtime-benchmarks",
	"pallet-timestamp/runtime-benchmarks",
	"pallet-treasury/runtime-benchmarks",
	"sc-client-db/runtime-benchmarks",
	"sc-service/runtime-benchmarks",
	"sp-runtime/runtime-benchmarks",
]
# Enable features that allow the runtime to be tried and debugged. Name might be subject to change
# in the near future.
try-runtime = [
	"frame-support/try-runtime",
	"frame-system/try-runtime",
	"peerplays-runtime/try-runtime",
	"pallet-asset-conversion-tx-payment/try-runtime",
	"pallet-asset-tx-payment/try-runtime",
	"pallet-assets/try-runtime",
	"pallet-balances/try-runtime",
	"pallet-contracts/try-runtime",
	#"pallet-glutton/try-runtime",
	"pallet-im-online/try-runtime",
	"pallet-root-testing/try-runtime",
	"pallet-skip-feeless-payment/try-runtime",
	"pallet-sudo/try-runtime",
	"pallet-timestamp/try-runtime",
	"pallet-transaction-payment/try-runtime",
	"pallet-treasury/try-runtime",
	"sp-runtime/try-runtime",
	#"substrate-cli-test-utils/try-runtime",
	"try-runtime-cli/try-runtime",
]

#[[bench]]
#name = "transaction_pool"
#harness = false

[[bench]]
name = "block_production"
harness = false

[[bench]]
name = "executor"
harness = false

// This file is part of Peerplays 2.0.

// Copyright (C) Parity Technologies (UK) Ltd.
// SPDX-License-Identifier: GPL-3.0-or-later WITH Classpath-exception-2.0

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

//! Peerplays 2.0 chain configurations.

use beefy_primitives::ecdsa_crypto::AuthorityId as BeefyId;
use grandpa_primitives::AuthorityId as GrandpaId;
use pallet_im_online::sr25519::AuthorityId as ImOnlineId;
use sc_chain_spec::ChainSpecExtension;
use sc_service::ChainType;
use sc_telemetry::TelemetryEndpoints;
use serde::{Deserialize, Serialize};
use sp_authority_discovery::AuthorityId as AuthorityDiscoveryId;
use sp_consensus_babe::AuthorityId as BabeId;
use sp_core::{Pair, Public};
use sp_mixnet::types::AuthorityId as MixnetId;
use sp_runtime::{
	traits::{IdentifyAccount, Verify},
	Perbill,
};

pub use node_primitives::{AccountId, Balance, Signature};
pub use peerplays_runtime::RuntimeGenesisConfig;

type AccountPublic = <Signature as Verify>::Signer;

use peerplays_runtime::{EVMChainIdConfig, PalletEVM::pallet::GenesisConfig as EVMConfig};
use sc_chain_spec::Properties;
use sp_core::{ecdsa, H160, U256};
use std::{collections::BTreeMap, str::FromStr};

use peerplays_runtime::{
	constants::currency::*, wasm_binary_unwrap, AuthorityDiscoveryConfig, BabeConfig,
	BalancesConfig, Block, CouncilConfig, DemocracyConfig, ElectionsConfig, GrandpaConfig,
	ImOnlineConfig, IndicesConfig, MaxNominations, NominationPoolsConfig, SessionConfig,
	SessionKeys, SocietyConfig, StakerStatus, StakingConfig, SudoConfig, SystemConfig,
	TechnicalCommitteeConfig,
};

const STAGING_TELEMETRY_URL: &str = "wss://telemetry.polkadot.io/submit/";
const ENDOWMENT: Balance = 10_000_000 * PPY;
const STASH: Balance = ENDOWMENT / 1000;

/// Node `ChainSpec` extensions.
///
/// Additional parameters for some Peerplays 2.0 core modules,
/// customizable from the chain spec.
#[derive(Default, Clone, Serialize, Deserialize, ChainSpecExtension)]
#[serde(rename_all = "camelCase")]
pub struct Extensions {
	/// Block numbers with known hashes.
	pub fork_blocks: sc_client_api::ForkBlocks<Block>,
	/// Known bad block hashes.
	pub bad_blocks: sc_client_api::BadBlocks<Block>,
	/// The light sync state extension used by the sync-state rpc.
	pub light_sync_state: sc_sync_state_rpc::LightSyncStateExtension,
}

/// Specialized `ChainSpec`.
pub type ChainSpec = sc_service::GenericChainSpec<RuntimeGenesisConfig, Extensions>;

fn session_keys(
	grandpa: GrandpaId,
	babe: BabeId,
	im_online: ImOnlineId,
	authority_discovery: AuthorityDiscoveryId,
	mixnet: MixnetId,
	beefy: BeefyId,
) -> SessionKeys {
	SessionKeys { grandpa, babe, im_online, authority_discovery, mixnet, beefy }
}

/// Helper function to generate a crypto pair from seed
pub fn get_from_seed<TPublic: Public>(seed: &str) -> <TPublic::Pair as Pair>::Public {
	TPublic::Pair::from_string(seed, None)
		.expect("static values are valid; qed")
		.public()
}

/// Helper function to generate an account ID from seed
pub fn get_account_id_from_seed<TPublic: Public>(seed: &str) -> AccountId
where
	AccountPublic: From<<TPublic::Pair as Pair>::Public>,
{
	AccountPublic::from(get_from_seed::<TPublic>(seed)).into_account()
}

/// Helper function to generate stash, controller and session key from seed
pub fn authority_keys_from_seed(
	seed: &str,
) -> (AccountId, AccountId, GrandpaId, BabeId, ImOnlineId, AuthorityDiscoveryId, MixnetId, BeefyId)
{
	(
		get_account_id_from_seed::<ecdsa::Public>(&format!("{}//stash", seed)),
		get_account_id_from_seed::<ecdsa::Public>(seed),
		get_from_seed::<GrandpaId>(seed),
		get_from_seed::<BabeId>(seed),
		get_from_seed::<ImOnlineId>(seed),
		get_from_seed::<AuthorityDiscoveryId>(seed),
		get_from_seed::<MixnetId>(seed),
		get_from_seed::<BeefyId>(seed),
	)
}

/// Helper function to create RuntimeGenesisConfig for testing
pub fn default_genesis(
	initial_authorities: Vec<(
		AccountId,
		AccountId,
		GrandpaId,
		BabeId,
		ImOnlineId,
		AuthorityDiscoveryId,
		MixnetId,
		BeefyId,
	)>,
	initial_nominators: Vec<AccountId>,
	root_key: AccountId,
	endowed_accounts: Vec<AccountId>,
	eth_chain_id: u64,
	endowed_eth_accounts: BTreeMap<H160, fp_evm::GenesisAccount>,
) -> RuntimeGenesisConfig {
	// stakers: all validators and nominators.
	let mut rng = rand::thread_rng();
	let stakers = initial_authorities
		.iter()
		.map(|x| (x.0.clone(), x.1.clone(), STASH, StakerStatus::Validator))
		.chain(initial_nominators.iter().map(|x| {
			use rand::{seq::SliceRandom, Rng};
			let limit = (MaxNominations::get() as usize).min(initial_authorities.len());
			let count = rng.gen::<usize>() % limit;
			let nominations = initial_authorities
				.as_slice()
				.choose_multiple(&mut rng, count)
				.into_iter()
				.map(|choice| choice.0.clone())
				.collect::<Vec<_>>();
			(x.clone(), x.clone(), STASH, StakerStatus::Nominator(nominations))
		}))
		.collect::<Vec<_>>();

	let num_endowed_accounts = endowed_accounts.len();

	RuntimeGenesisConfig {
		system: SystemConfig { ..Default::default() },
		balances: BalancesConfig {
			balances: endowed_accounts.iter().cloned().map(|x| (x, ENDOWMENT)).collect(),
		},
		indices: IndicesConfig { indices: vec![] },
		session: SessionConfig {
			keys: initial_authorities
				.iter()
				.map(|x| {
					(
						x.0.clone(),
						x.0.clone(),
						session_keys(
							x.2.clone(),
							x.3.clone(),
							x.4.clone(),
							x.5.clone(),
							x.6.clone(),
							x.7.clone(),
						),
					)
				})
				.collect::<Vec<_>>(),
		},
		staking: StakingConfig {
			validator_count: initial_authorities.len() as u32,
			minimum_validator_count: initial_authorities.len() as u32,
			invulnerables: initial_authorities.iter().map(|x| x.0.clone()).collect(),
			slash_reward_fraction: Perbill::from_percent(10),
			stakers,
			..Default::default()
		},
		democracy: DemocracyConfig::default(),
		elections: ElectionsConfig {
			members: endowed_accounts
				.iter()
				.take((num_endowed_accounts + 1) / 2)
				.cloned()
				.map(|member| (member, STASH))
				.collect(),
		},
		council: CouncilConfig::default(),
		technical_committee: TechnicalCommitteeConfig {
			members: endowed_accounts
				.iter()
				.take((num_endowed_accounts + 1) / 2)
				.cloned()
				.collect(),
			phantom: Default::default(),
		},
		sudo: SudoConfig { key: Some(root_key) },
		babe: BabeConfig {
			authorities: vec![],
			epoch_config: Some(peerplays_runtime::BABE_GENESIS_EPOCH_CONFIG),
			..Default::default()
		},
		im_online: ImOnlineConfig { keys: vec![] },
		authority_discovery: AuthorityDiscoveryConfig { keys: vec![], ..Default::default() },
		grandpa: GrandpaConfig { authorities: vec![], ..Default::default() },
		technical_membership: Default::default(),
		treasury: Default::default(),
		society: SocietyConfig { pot: 0 },
		vesting: Default::default(),
		assets: Default::default(),
		pool_assets: Default::default(),
		transaction_storage: Default::default(),
		transaction_payment: Default::default(),
		alliance: Default::default(),
		alliance_motion: Default::default(),
		nomination_pools: NominationPoolsConfig {
			min_create_bond: 10 * PPY,
			min_join_bond: 1 * PPY,
			..Default::default()
		},
		tx_pause: Default::default(),
		safe_mode: Default::default(),
		beefy: Default::default(),
		mixnet: Default::default(),

		// EVM compatibility
		evm_chain_id: EVMChainIdConfig { chain_id: eth_chain_id, ..Default::default() },
		evm: EVMConfig { accounts: endowed_eth_accounts, ..Default::default() },
		ethereum: Default::default(),
		dynamic_fee: Default::default(),
		base_fee: Default::default(),
	}
}

fn development_config_genesis() -> RuntimeGenesisConfig {
	let seed_a = "bottom drive obey lake curtain smoke basket hold race lonely fit walk//Alice";
	let seed_b = "bottom drive obey lake curtain smoke basket hold race lonely fit walk//Bob";
	let seed_c = "bottom drive obey lake curtain smoke basket hold race lonely fit walk//Charlie";
	let seed_d = "bottom drive obey lake curtain smoke basket hold race lonely fit walk//Dave";
	let seed_e = "bottom drive obey lake curtain smoke basket hold race lonely fit walk//Eve";
	let seed_f = "bottom drive obey lake curtain smoke basket hold race lonely fit walk//Ferdie";

	let alice = authority_keys_from_seed(seed_a);
	let bob = authority_keys_from_seed(seed_b);
	let charlie = authority_keys_from_seed(seed_c);
	let dave = authority_keys_from_seed(seed_d);
	let eve = authority_keys_from_seed(seed_e);
	let ferdie = authority_keys_from_seed(seed_f);

	let initial_authorities = vec![alice.clone()];

	let initial_nominators = vec![];

	let root_key = alice.1.clone();

	let endowed_accounts = vec![
		alice.0.clone(),
		bob.0.clone(),
		charlie.0.clone(),
		dave.0.clone(),
		eve.0.clone(),
		ferdie.0.clone(),
		alice.1.clone(),
		bob.1.clone(),
		charlie.1.clone(),
		dave.1.clone(),
		eve.1.clone(),
		ferdie.1.clone(),
	];

	let mut endowed_eth_accounts = BTreeMap::new();
	endowed_eth_accounts.insert(
		// Alice
		H160::from_str("0xE04CC55ebEE1cBCE552f250e85c57B70B2E2625b")
			.expect("internal H160 is valid; qed"),
		fp_evm::GenesisAccount {
			nonce: Default::default(),
			balance: U256::from(1_000_000_000_000_000_000_000_000u128),
			storage: Default::default(),
			code: Default::default(),
		},
	);
	endowed_eth_accounts.insert(
		// Alith
		H160::from_str("0xf24FF3a9CF04c71Dbc94D0b566f7A27B94566cac")
			.expect("internal H160 is valid; qed"),
		fp_evm::GenesisAccount {
			nonce: Default::default(),
			balance: U256::from(1_000_000_000_000_000_000_000_000u128),
			storage: Default::default(),
			code: Default::default(),
		},
	);
	endowed_eth_accounts.insert(
		H160::from_str("0x5fbbb31be52608d2f52247e8400b7fcaa9e0bc12")
			.expect("internal H160 is valid; qed"),
		fp_evm::GenesisAccount {
			nonce: Default::default(),
			balance: U256::from(1_000_000_000_000_000_000_000_000u128),
			storage: Default::default(),
			code: Default::default(),
		},
	);

	default_genesis(
		initial_authorities,
		initial_nominators,
		root_key,
		endowed_accounts,
		33,
		endowed_eth_accounts,
	)
}

fn testnet_config_genesis() -> RuntimeGenesisConfig {
	let seed00 = "main awake seek truly rebel pluck aunt receive height cook bless weasel";
	let seed01 = "use clock lunar there health private pigeon oyster trigger half evolve slab";
	let seed02 = "blanket faith tired fitness wrong mouse duck raise gun flee tiger also";
	let seed03 = "vault tilt guess neutral melody buddy embody priority wrong bronze mention juice";
	let seed04 = "dutch barely host rapid device fee define drip syrup clay come disease";
	let seed05 = "draw maid blast hurt anchor delay impulse task raw helmet sort brick";
	let seed06 = "beyond payment agree hat rapid acquire trouble fringe demise satoshi peace apart";
	let seed07 = "piano family squirrel glad oven remember impose skill wrong couple fade lizard";
	let seed08 = "warfare object accuse master wear wage tribe peace hospital maximum candy boy";
	let seed09 = "few sort attitude current exist render juice swift alcohol they energy setup";
	let seed10 = "soccer quote outside moment potato creek tenant enforce marble feel venue apart";

	let init00 = authority_keys_from_seed(seed00);
	let init01 = authority_keys_from_seed(seed01);
	let init02 = authority_keys_from_seed(seed02);
	let init03 = authority_keys_from_seed(seed03);
	let init04 = authority_keys_from_seed(seed04);
	let init05 = authority_keys_from_seed(seed05);
	let init06 = authority_keys_from_seed(seed06);
	let init07 = authority_keys_from_seed(seed07);
	let init08 = authority_keys_from_seed(seed08);
	let init09 = authority_keys_from_seed(seed09);
	let init10 = authority_keys_from_seed(seed10);

	let initial_authorities = vec![
		init00.clone(),
		init01.clone(),
		init02.clone(),
		init03.clone(),
		init04.clone(),
		init05.clone(),
		init06.clone(),
		init07.clone(),
		init08.clone(),
		init09.clone(),
		init10.clone(),
	];

	let initial_nominators = vec![];

	let root_key = init00.1.clone();

	let endowed_accounts = vec![
		init00.0.clone(),
		init01.0.clone(),
		init02.0.clone(),
		init03.0.clone(),
		init04.0.clone(),
		init05.0.clone(),
		init06.0.clone(),
		init07.0.clone(),
		init08.0.clone(),
		init09.0.clone(),
		init10.0.clone(),
		init00.1.clone(),
		init01.1.clone(),
		init02.1.clone(),
		init03.1.clone(),
		init04.1.clone(),
		init05.1.clone(),
		init06.1.clone(),
		init07.1.clone(),
		init08.1.clone(),
		init09.1.clone(),
		init10.1.clone(),
	];

	let mut endowed_eth_accounts = BTreeMap::new();
	endowed_eth_accounts.insert(
		// init00
		H160::from_str("0xD950736a02C5a98567d7aE3d20A1A0b06B02cA14")
			.expect("internal H160 is valid; qed"),
		fp_evm::GenesisAccount {
			nonce: Default::default(),
			balance: U256::from(1_000_000_000_000_000_000_000_000u128),
			storage: Default::default(),
			code: Default::default(),
		},
	);

	default_genesis(
		initial_authorities,
		initial_nominators,
		root_key,
		endowed_accounts,
		33,
		endowed_eth_accounts,
	)
}

fn mainnet_config_genesis() -> RuntimeGenesisConfig {
	let seed00 = "main awake seek truly rebel pluck aunt receive height cook bless weasel";
	let seed01 = "use clock lunar there health private pigeon oyster trigger half evolve slab";
	let seed02 = "blanket faith tired fitness wrong mouse duck raise gun flee tiger also";
	let seed03 = "vault tilt guess neutral melody buddy embody priority wrong bronze mention juice";
	let seed04 = "dutch barely host rapid device fee define drip syrup clay come disease";
	let seed05 = "draw maid blast hurt anchor delay impulse task raw helmet sort brick";
	let seed06 = "beyond payment agree hat rapid acquire trouble fringe demise satoshi peace apart";
	let seed07 = "piano family squirrel glad oven remember impose skill wrong couple fade lizard";
	let seed08 = "warfare object accuse master wear wage tribe peace hospital maximum candy boy";
	let seed09 = "few sort attitude current exist render juice swift alcohol they energy setup";
	let seed10 = "soccer quote outside moment potato creek tenant enforce marble feel venue apart";

	let init00 = authority_keys_from_seed(seed00);
	let init01 = authority_keys_from_seed(seed01);
	let init02 = authority_keys_from_seed(seed02);
	let init03 = authority_keys_from_seed(seed03);
	let init04 = authority_keys_from_seed(seed04);
	let init05 = authority_keys_from_seed(seed05);
	let init06 = authority_keys_from_seed(seed06);
	let init07 = authority_keys_from_seed(seed07);
	let init08 = authority_keys_from_seed(seed08);
	let init09 = authority_keys_from_seed(seed09);
	let init10 = authority_keys_from_seed(seed10);

	let initial_authorities = vec![
		init00.clone(),
		init01.clone(),
		init02.clone(),
		init03.clone(),
		init04.clone(),
		init05.clone(),
		init06.clone(),
		init07.clone(),
		init08.clone(),
		init09.clone(),
		init10.clone(),
	];

	let initial_nominators = vec![];

	let root_key = init00.1.clone();

	let endowed_accounts = vec![
		init00.0.clone(),
		init01.0.clone(),
		init02.0.clone(),
		init03.0.clone(),
		init04.0.clone(),
		init05.0.clone(),
		init06.0.clone(),
		init07.0.clone(),
		init08.0.clone(),
		init09.0.clone(),
		init10.0.clone(),
		init00.1.clone(),
		init01.1.clone(),
		init02.1.clone(),
		init03.1.clone(),
		init04.1.clone(),
		init05.1.clone(),
		init06.1.clone(),
		init07.1.clone(),
		init08.1.clone(),
		init09.1.clone(),
		init10.1.clone(),
	];

	let mut endowed_eth_accounts = BTreeMap::new();
	endowed_eth_accounts.insert(
		// init00
		H160::from_str("0xD950736a02C5a98567d7aE3d20A1A0b06B02cA14")
			.expect("internal H160 is valid; qed"),
		fp_evm::GenesisAccount {
			nonce: Default::default(),
			balance: U256::from(1_000_000_000_000_000_000_000_000u128),
			storage: Default::default(),
			code: Default::default(),
		},
	);

	default_genesis(
		initial_authorities,
		initial_nominators,
		root_key,
		endowed_accounts,
		33,
		endowed_eth_accounts,
	)
}

/// Development config
pub fn development_config() -> ChainSpec {
	let mut properties: Properties = Properties::new();
	properties.insert("tokenDecimals".into(), 18.into());
	properties.insert("tokenSymbol".into(), "TEST".into());

	#[allow(deprecated)]
	ChainSpec::from_genesis(
		"Peerplays 2.0",
		"dev",
		ChainType::Development,
		development_config_genesis,
		vec![],
		None,
		None,
		None,
		Some(properties),
		Default::default(),
		wasm_binary_unwrap(),
	)
}

/// Testnet config
pub fn testnet_config() -> ChainSpec {
	let mut properties: Properties = Properties::new();
	properties.insert("tokenDecimals".into(), 18.into());
	properties.insert("tokenSymbol".into(), "TEST".into());

	#[allow(deprecated)]
	ChainSpec::from_genesis(
		"Peerplays 2.0",
		"testnet",
		ChainType::Local,
		testnet_config_genesis,
		vec![],
		None,
		None,
		None,
		Some(properties),
		Default::default(),
		wasm_binary_unwrap(),
	)
}

/// Mainnet config
pub fn mainnet_config() -> ChainSpec {
	let mut properties: Properties = Properties::new();
	properties.insert("tokenDecimals".into(), 18.into());
	properties.insert("tokenSymbol".into(), "PPY".into());

	#[allow(deprecated)]
	ChainSpec::from_genesis(
		"Peerplays 2.0",
		"mainnet",
		ChainType::Live,
		mainnet_config_genesis,
		vec![],
		Some(
			TelemetryEndpoints::new(vec![(STAGING_TELEMETRY_URL.to_string(), 0)])
				.expect("Staging telemetry url is valid; qed"),
		),
		None,
		None,
		Some(properties),
		Default::default(),
		wasm_binary_unwrap(),
	)
}

#[cfg(test)]
pub(crate) mod tests {
	use super::*;
	use crate::{
		service::{new_full_base, NewFullBase},
		Cli,
	};
	use sc_cli::SubstrateCli;
	use sc_service_test;
	use sp_runtime::BuildStorage;

	fn local_testnet_genesis() -> RuntimeGenesisConfig {
		let endowed_eth_accounts = BTreeMap::new();
		default_genesis(
			vec![authority_keys_from_seed("//Alice"), authority_keys_from_seed("//Bob")],
			vec![],
			get_account_id_from_seed::<ecdsa::Public>("//Alice"),
			vec![],
			33,
			endowed_eth_accounts,
		)
	}

	fn local_testnet_genesis_instant_single() -> RuntimeGenesisConfig {
		let endowed_eth_accounts = BTreeMap::new();
		default_genesis(
			vec![authority_keys_from_seed("//Alice")],
			vec![],
			get_account_id_from_seed::<ecdsa::Public>("//Alice"),
			vec![],
			33,
			endowed_eth_accounts,
		)
	}

	/// Local testnet config (single validator - Alice)
	pub fn integration_test_config_with_single_authority() -> ChainSpec {
		#[allow(deprecated)]
		ChainSpec::from_genesis(
			"Integration Test",
			"test",
			ChainType::Development,
			local_testnet_genesis_instant_single,
			vec![],
			None,
			None,
			None,
			None,
			Default::default(),
			wasm_binary_unwrap(),
		)
	}

	/// Local testnet config (multivalidator Alice + Bob)
	pub fn integration_test_config_with_two_authorities() -> ChainSpec {
		#[allow(deprecated)]
		ChainSpec::from_genesis(
			"Integration Test",
			"test",
			ChainType::Development,
			local_testnet_genesis,
			vec![],
			None,
			None,
			None,
			None,
			Default::default(),
			wasm_binary_unwrap(),
		)
	}

	#[test]
	#[ignore]
	fn test_connectivity() {
		sp_tracing::try_init_simple();
		let cli = Cli::from_args();

		sc_service_test::connectivity(integration_test_config_with_two_authorities(), |config| {
			let NewFullBase { task_manager, client, network, sync, transaction_pool, .. } =
				new_full_base(config, &cli, |_, _| ())?;
			Ok(sc_service_test::TestNetComponents::new(
				task_manager,
				client,
				network,
				sync,
				transaction_pool,
			))
		});
	}

	#[test]
	fn test_create_development_chain_spec() {
		development_config().build_storage().unwrap();
	}

	#[test]
	fn test_create_testnet_chain_spec() {
		testnet_config().build_storage().unwrap();
	}

	#[test]
	fn test_create_mainnet_chain_spec() {
		mainnet_config().build_storage().unwrap();
	}
}

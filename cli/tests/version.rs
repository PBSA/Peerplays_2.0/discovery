// This file is part of Peerplays 2.0.

// Copyright (C) Parity Technologies (UK) Ltd.
// SPDX-License-Identifier: GPL-3.0-or-later WITH Classpath-exception-2.0

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use assert_cmd::cargo::cargo_bin;
use regex::Regex;
use std::process::Command;

fn expected_regex() -> Regex {
	Regex::new(r"^peerplays (.+)-([a-f\d]+)$").unwrap()
}

#[test]
fn version_is_full() {
	let expected = expected_regex();
	let output = Command::new(cargo_bin("peerplays")).args(&["--version"]).output().unwrap();

	assert!(output.status.success(), "command returned with non-success exit code");

	let output = dbg!(String::from_utf8_lossy(&output.stdout).trim().to_owned());
	let captures = expected.captures(output.as_str()).expect("could not parse version in output");

	assert_eq!(&captures[1], env!("CARGO_PKG_VERSION"));
}

#[test]
fn test_regex_matches_properly() {
	let expected = expected_regex();

	let captures = expected.captures("peerplays 3.0.0-f3c59099b47").unwrap();
	assert_eq!(&captures[1], "3.0.0");
	assert_eq!(&captures[2], "f3c59099b47");

	let captures = expected.captures("peerplays 3.0.0-dev-f3c59099b47").unwrap();
	assert_eq!(&captures[1], "3.0.0-dev");
	assert_eq!(&captures[2], "f3c59099b47");
}

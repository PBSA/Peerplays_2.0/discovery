# Peerplays 2.0 QA environment

This set of docker images contains self-contained, ready to go, Peerplays 2.0 QA environment.

Features:
- Peerplays 2.0 Private Testnet Multi Node network - 11 Peerplays 2.0 nodes, running 11 validators
- Private Docker network used for connecting all containers
- Polkadot UI, customized for Peerplays 2.0
- Contracts UI, to handle Ink contracts
- Substrate Telemetry, to collect telemetry data from Peerplays 2.0 nodes
- Prometheus, to collect telemetry data from Peerplays 2.0 nodes
- Grafana to visualize telemetry data from Prometheus
- Funded substrate accounts
- - For dev: Alice
- - For testnet: init00, init01, init02, init03, init04, init05, init06, init07, init08, init09, init10
- - For mainnet: init00, init01, init02, init03, init04, init05, init06, init07, init08, init09, init10
- Funded Eth accounts
- - For dev: Alice
- - For testnet: init00
- - For mainnet: init00

The software used to run Peerplays 2.0 is the latest docker image from the master branch in the discovery repository.

## Building

### Full build

The images can be build using the command bellow. It will create 11 images ready to be started:
```
docker-compose build
```

### Partial build
There is also an option to build each docker image separately. The separate images can be build using (1 call for each image):
```
docker-compose build discovery-polkadot-ui
docker-compose build discovery-contracts-ui
docker-compose build discovery-grafana
docker-compose build discovery-substrate-telemetry-core
docker-compose build discovery-substrate-telemetry-shard
docker-compose build discovery-substrate-telemetry-frontend
docker-compose build discovery-validator-alice
docker-compose build discovery-validator-bob
docker-compose build discovery-validator-charlie
docker-compose build discovery-validator-dave
docker-compose build discovery-validator-eve
docker-compose build discovery-validator-fredie
docker-compose build discovery-validator-grace
docker-compose build discovery-validator-heidi
docker-compose build discovery-validator-ivan
docker-compose build discovery-validator-judy
docker-compose build discovery-validator-mallory
```

## Running a custom image

The QA environment allows you to use a custom image as `discovery-base` image and test the behavior of the blockchain with unpublished changes. In order to use a custom discovery docker image, such image has to be published in https://gitlab.com/PBSA/Peerplays_2.0/discovery/container_registry. The image is published based on the branch name and commit SHA. In order to publish a docker image, you have to manually run the dockerize job in your MR pipeline.

Once the docker image is published in the container registry, it can be used in the QA environment by modifying the first line in [Dockerfile](Dockerfile) in the below format (<branch_name> and <commit_SHA> should be substitute with the actual values):
```
FROM registry.gitlab.com/pbsa/peerplays_2.0/discovery/<branch_name>:<commit_SHA>
```

After modifying the FROM statement, the docker containers have to be rebuilt using new base image. You can rebuild the docker containers by executing:
```
docker-compose build --no-cache
```

After the containers are rebuilt, they can be run following instructions given in [starting nodes](#starting-nodes).

## Starting nodes

### Full start
All 11 nodes can be started with a single command line call:
```
docker-compose up
```

### Partial start
Each of the nodes can be started with a separate call using (1 call for each node):
```
docker-compose up discovery-polkadot-ui
docker-compose up discovery-contracts-ui
docker-compose up discovery-prometheus
docker-compose up discovery-grafana
docker-compose up discovery-substrate-telemetry-core
docker-compose up discovery-substrate-telemetry-shard
docker-compose up discovery-substrate-telemetry-frontend
docker-compose up discovery-validator-alice
docker-compose up discovery-validator-bob
docker-compose up discovery-validator-charlie
docker-compose up discovery-validator-dave
docker-compose up discovery-validator-eve
docker-compose up discovery-validator-fredie
docker-compose up discovery-validator-grace
docker-compose up discovery-validator-heidi
docker-compose up discovery-validator-ivan
docker-compose up discovery-validator-judy
docker-compose up discovery-validator-mallory
```

## Confirmation
If enough nodes are started (more than 8), and they can successfully communicate with each other, we should start seeing finalized blocks. Example:
```
discovery-validator-grace_1    | 2023-05-03 10:51:07 💤 Idle (10 peers), best: #5 (0x52a5…77cd), finalized #3 (0xff76…a8fc), ⬇ 54.3kiB/s ⬆ 50.8kiB/s
```

## Stopping nodes
Nodes can be stopped using the below command:
```
docker-compose down
```

// This file is part of Peerplays 2.0.

// Copyright (C) Parity Technologies (UK) Ltd.
// SPDX-License-Identifier: GPL-3.0-or-later WITH Classpath-exception-2.0

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

//! Genesis Configuration.

use crate::keyring::*;
use peerplays_runtime::{
	constants::currency::*, AccountId, AssetsConfig, BabeConfig, BalancesConfig, GrandpaConfig,
	IndicesConfig, RuntimeGenesisConfig, SessionConfig, SocietyConfig, StakerStatus, StakingConfig,
	BABE_GENESIS_EPOCH_CONFIG,
};
use sp_keyring::Ed25519Keyring;
use sp_runtime::Perbill;
use std::{collections::BTreeMap, str::FromStr};

use sp_core::{H160, U256};

/// Create genesis runtime configuration for tests.
pub fn config() -> RuntimeGenesisConfig {
	config_endowed(Default::default())
}

/// Create genesis runtime configuration for tests with some extra
/// endowed accounts.
pub fn config_endowed(extra_endowed: Vec<AccountId>) -> RuntimeGenesisConfig {
	let mut endowed = vec![
		(alice(), 111 * PPY),
		(bob(), 100 * PPY),
		(charlie(), 100_000_000 * PPY),
		(dave(), 111 * PPY),
		(eve(), 101 * PPY),
		(ferdie(), 100 * PPY),
	];

	use peerplays_runtime::{EVMChainIdConfig, PalletEVM::pallet::GenesisConfig as EVMConfig};
	let eth_chain_id: u64 = 0;

	endowed.extend(extra_endowed.into_iter().map(|endowed| (endowed, 100 * PPY)));

	RuntimeGenesisConfig {
		system: Default::default(),
		indices: IndicesConfig { indices: vec![] },
		balances: BalancesConfig { balances: endowed },
		session: SessionConfig {
			keys: vec![
				(alice(), dave(), to_session_keys(Ed25519Keyring::Alice.into())),
				(bob(), eve(), to_session_keys(Ed25519Keyring::Bob.into())),
				(charlie(), ferdie(), to_session_keys(Ed25519Keyring::Charlie.into())),
			],
		},
		staking: StakingConfig {
			stakers: vec![
				(dave(), dave(), 111 * PPY, StakerStatus::Validator),
				(eve(), eve(), 100 * PPY, StakerStatus::Validator),
				(ferdie(), ferdie(), 100 * PPY, StakerStatus::Validator),
			],
			validator_count: 3,
			minimum_validator_count: 0,
			slash_reward_fraction: Perbill::from_percent(10),
			invulnerables: vec![alice(), bob(), charlie()],
			..Default::default()
		},
		babe: BabeConfig {
			authorities: vec![],
			epoch_config: Some(BABE_GENESIS_EPOCH_CONFIG),
			..Default::default()
		},
		grandpa: GrandpaConfig { authorities: vec![], ..Default::default() },
		beefy: Default::default(),
		im_online: Default::default(),
		authority_discovery: Default::default(),
		democracy: Default::default(),
		council: Default::default(),
		technical_committee: Default::default(),
		technical_membership: Default::default(),
		elections: Default::default(),
		sudo: Default::default(),
		treasury: Default::default(),
		society: SocietyConfig { pot: 0 },
		vesting: Default::default(),
		assets: AssetsConfig { assets: vec![(9, alice(), true, 1)], ..Default::default() },
		pool_assets: Default::default(),
		transaction_storage: Default::default(),
		transaction_payment: Default::default(),
		alliance: Default::default(),
		alliance_motion: Default::default(),
		nomination_pools: Default::default(),
		safe_mode: Default::default(),
		tx_pause: Default::default(),
		mixnet: Default::default(),

		// EVM compatibility
		evm_chain_id: EVMChainIdConfig { chain_id: eth_chain_id, ..Default::default() },
		evm: EVMConfig {
			accounts: {
				let mut map = BTreeMap::new();
				map.insert(
					// H160 address of Alice dev account
					// Derived from SS58 (42 prefix) address
					// SS58: 5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY
					// hex: 0xd43593c715fdd31c61141abd04a99fd6822c8558854ccde39a5684e7a56da27d
					// Using the full hex key, truncating to the first 20 bytes (the first 40 hex
					// chars)
					H160::from_str("d43593c715fdd31c61141abd04a99fd6822c8558")
						.expect("internal H160 is valid; qed"),
					fp_evm::GenesisAccount {
						balance: U256::from_str("0xffffffffffffffffffffffffffffffff")
							.expect("internal U256 is valid; qed"),
						code: Default::default(),
						nonce: Default::default(),
						storage: Default::default(),
					},
				);
				map.insert(
					// H160 address of CI test runner account
					H160::from_str("6be02d1d3665660d22ff9624b7be0551ee1ac91b")
						.expect("internal H160 is valid; qed"),
					fp_evm::GenesisAccount {
						balance: U256::from_str("0xffffffffffffffffffffffffffffffff")
							.expect("internal U256 is valid; qed"),
						code: Default::default(),
						nonce: Default::default(),
						storage: Default::default(),
					},
				);
				map.insert(
					// H160 address for benchmark usage
					H160::from_str("1000000000000000000000000000000000000001")
						.expect("internal H160 is valid; qed"),
					fp_evm::GenesisAccount {
						nonce: U256::from(1),
						balance: U256::from(1_000_000_000_000_000_000_000_000u128),
						storage: Default::default(),
						code: vec![0x00],
					},
				);
				map
			},
			..Default::default()
		},
		ethereum: Default::default(),
		dynamic_fee: Default::default(),
		base_fee: Default::default(),
	}
}
